//
//  SnakeNodeView.swift
//  snakedemo
//
//  Created by Allejo Chris Velarde on 5/6/16.
//  Copyright © 2016 Allejo Chris Velarde. All rights reserved.
//

import Foundation
import UIKit

class SnakeNodeView: UIView
{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.purple
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.purple
    }
    
    override func draw(_ rect: CGRect) {
        
        let inner = CGRect(origin: CGPoint(x: rect.origin.x+1, y: rect.origin.y+1), size: CGSize(width: rect.size.width-2, height: rect.height-2))
        UIColor.green.setFill()
        UIBezierPath(rect: inner).fill()
    }
    
    func moveToPoint(_ point: Point)
    {
        self.frame.origin.x = CGFloat(point.x * SnakeNodeSize)
        self.frame.origin.y = CGFloat(point.y * SnakeNodeSize)
    }
}
