//
//  FoodView.swift
//  snakedemo
//
//  Created by Allejo Chris Velarde on 5/9/16.
//  Copyright © 2016 Allejo Chris Velarde. All rights reserved.
//

import Foundation
import UIKit

class Food
{
    var foodView: FoodView!
    var point: Point
    
    init(tilePosition: Point)
    {
        self.point = tilePosition
        
        // create view
        let rect = CGRect(x: self.point.x * SnakeNodeSize, y: self.point.y * SnakeNodeSize, width: SnakeNodeSize, height: SnakeNodeSize)
        self.foodView = FoodView(frame: rect)
    }
    
    func hide()
    {
        self.foodView.isHidden = true
    }
    
    func show()
    {
        self.foodView.isHidden = false
    }
    
    func move(_ newPoint: Point)
    {
        self.point = newPoint
        self.foodView.frame.origin.x = CGFloat(self.point.x * SnakeNodeSize)
        self.foodView.frame.origin.y = CGFloat(self.point.y * SnakeNodeSize)
    }
}

class FoodView: UIView
{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.purple
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.purple
    }
    
    override func draw(_ rect: CGRect) {
        
        let inner = CGRect(origin: CGPoint(x: rect.origin.x+1, y: rect.origin.y+1), size: CGSize(width: rect.size.width-2, height: rect.height-2))
        UIColor.red.setFill()
        UIBezierPath(rect: inner).fill()
    }
}
