//
//  SnakeNode.swift
//  snakedemo
//
//  Created by Allejo Chris Velarde on 5/6/16.
//  Copyright © 2016 Allejo Chris Velarde. All rights reserved.
//

import Foundation
import UIKit

public let SnakeNodeSize = 20

// Tile position
struct Point {
    var x: Int
    var y: Int
}

extension Point {
    func isEqual(_ comparison: Point) -> Bool
    {
        if self.x == comparison.x && self.y == comparison.y {
            return true
        }
        return false
    }
}

class SnakeNode
{
    var next: SnakeNode?
    
    var position: Int = 0
    var point: Point!
    
    // just to remember the previous location from last cycle
    var lastPoint: Point?
    
    var nodeView: SnakeNodeView?
    
    init (pos: Int) {
        self.position = pos
    }
    
    init (pos: Int, tileLocation: Point) {
        self.position = pos
        
        self.point = tileLocation
        
        // since we have the tile location we create the node view for this
        let rect = CGRect(x: self.point.x * SnakeNodeSize, y: self.point.y * SnakeNodeSize, width: SnakeNodeSize, height: SnakeNodeSize)
        self.nodeView = SnakeNodeView(frame: rect)
    }
    
    func update()
    {
        self.nodeView?.moveToPoint(self.point)
    }
}
