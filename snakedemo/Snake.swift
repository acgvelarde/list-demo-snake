//
//  Snake.swift
//  snakedemo
//
//  Created by Allejo Chris Velarde on 5/6/16.
//  Copyright © 2016 Allejo Chris Velarde. All rights reserved.
//

import Foundation
import UIKit

protocol SnakeWorldDelegate
{
    var snakeWorldView: UIView! { get }
}

class Snake
{
    var headNode: SnakeNode!
    var countNodes: Int = 1
    var worldDelegate: SnakeWorldDelegate!
    
    
    init(startingPoint: Point){
        // create head node
        self.headNode = SnakeNode(pos: 1, tileLocation: startingPoint)
        
    }
    
    // Add SnakeNode to tail
    func addNode(_ nodeToAdd: SnakeNode)
    {
        guard let lastNode = self.getLastNode() else {
            return
        }
        
        self.countNodes += 1
        lastNode.next = nodeToAdd
        
    }
    
    // convenience for creating a node and adding it to tail
    func grow()
    {
        guard let lastNode = self.getLastNode() else {
            return
        }
        
        let nodeToAdd = SnakeNode(pos: lastNode.position+1, tileLocation: lastNode.lastPoint!)
        self.countNodes += 1
        lastNode.next = nodeToAdd
        
        // add to world
        self.worldDelegate.snakeWorldView.addSubview(nodeToAdd.nodeView!)
    }
    
    func getLastNode() -> SnakeNode? {
        
        var currentNode = self.headNode
        while currentNode?.next != nil {
            
            currentNode = currentNode?.next
        }
        
        return currentNode
    }
    
    func move(_ direction: Movement)
    {
        // when moving we set the point of the next node from head
        var currentNode = self.headNode
        
        currentNode?.lastPoint = currentNode?.point
        
        while currentNode?.next != nil {
            
            // remember next node's last point
            currentNode?.next?.lastPoint = currentNode?.next?.point
            // set next node's point to current node's last point
            currentNode?.next?.point = currentNode?.lastPoint
            
            // set next as current node and rerender view
            currentNode = currentNode?.next
            currentNode?.update()
        }
        
        // move the head node
        switch direction {
        case .Left:
            self.headNode.point.x -= 1
        case .Right:
            self.headNode.point.x += 1
        case .Up:
            self.headNode.point.y -= 1
        case .Down:
            self.headNode.point.y += 1
        }
        self.headNode.update()
    }
    
}
