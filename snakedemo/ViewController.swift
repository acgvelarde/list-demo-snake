//
//  ViewController.swift
//  snakedemo
//
//  Created by Allejo Chris Velarde on 5/6/16.
//  Copyright © 2016 Allejo Chris Velarde. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func onStartGame(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "startToGame", sender: nil)
    }
}

