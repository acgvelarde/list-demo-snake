//
//  GameController.swift
//  snakedemo
//
//  Created by Allejo Chris Velarde on 5/6/16.
//  Copyright © 2016 Allejo Chris Velarde. All rights reserved.
//

import Foundation
import UIKit

enum Movement: String {
    case Up, Down, Right, Left
}

class GameController: UIViewController, SnakeWorldDelegate
{
    var snake: Snake!
    var timer: Timer!
    var food: Food!
    var snakeWorldView: UIView! {
        get {
            return self.worldView
        }
    }
    var score: Int = 0
    
    @IBOutlet weak var worldView: UIView!
    @IBOutlet weak var worldViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var worldViewWidthConstraint: NSLayoutConstraint!
    
    var worldWidth: Int = 0
    var worldHeight: Int = 0
    var movementDirection: Movement = Movement.Left
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // register gesture
        for direction in [UISwipeGestureRecognizerDirection.right, UISwipeGestureRecognizerDirection.left, UISwipeGestureRecognizerDirection.up, UISwipeGestureRecognizerDirection.down] {
            let gr = UISwipeGestureRecognizer(target: self, action: #selector(GameController.swipe(_:)))
            gr.direction = direction
            self.view.addGestureRecognizer(gr)
        }
        
        // initialize the world view
        self.worldView.backgroundColor = UIColor.black
        self.worldWidth = Int( Float(self.view.bounds.width) / Float(SnakeNodeSize) )
        self.worldHeight = Int( Float(self.view.bounds.height) / Float(SnakeNodeSize) )
        self.worldHeight -= 2
        self.worldViewWidthConstraint.constant = CGFloat(worldWidth * SnakeNodeSize)
        self.worldViewHeightConstraint.constant = CGFloat((worldHeight) * SnakeNodeSize)
        
        // create the snake, we render the head at the right most bottom
        self.snake = Snake(startingPoint: Point(x: worldWidth-5, y: worldHeight-1))
        self.snake.worldDelegate = self
        
        // add the head to worldview
        self.worldView.addSubview(self.snake.headNode.nodeView!)
        
        // create 4 more body nodes, then add to world view
        var previousPoint = self.snake.headNode.point
        for pos in 2...5 {
            let node = SnakeNode(pos: pos, tileLocation: Point(x: (previousPoint?.x)!+1, y: (previousPoint?.y)!))
            self.snake.addNode(node)
            
            // add to world view
            self.worldView.addSubview(node.nodeView!)
            previousPoint = node.point
        }
        
        // create the food  at the center
        self.food = Food(tilePosition: Point(x: worldWidth/2, y: worldHeight/2))
        //self.food = Food(tilePosition: Point(x: self.snake.headNode.point.x-5, y: self.snake.headNode.point.y))
        self.worldView.addSubview(self.food.foodView)
        
        
        
        // set up timer method
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(GameController.timerMethod(_:)), userInfo: nil, repeats: true)
    }
    
    func timerMethod(_ timer: Timer)
    {
        self.snake.move(self.movementDirection)
        
        if self.didHitFood() {
            // grow snake
            self.food.hide()
            self.snake.grow()
            
            // generate random point for our food
            self.food.move(self.generateFoodPoint())
            self.food.show()
            
            self.score += 1
        }
        
        if self.didHitWall() {
            endGame()
        }
        
        if self.didHitBody() {
            endGame()
        }
        
        
        
        
    }
    
    func generateFoodPoint() -> Point
    {
        var newPoint = Point(x: 0, y: 0)
        
        while (true) {
            newPoint.x = Int(arc4random_uniform(UInt32(worldWidth))) % worldWidth
            newPoint.y = Int(arc4random_uniform(UInt32(worldHeight))) % worldHeight
            
            // check if the new point intersects the snake
            var currentNode = self.snake.headNode
            var withinBody  = false
            while currentNode != nil {
                
                if (currentNode?.point.isEqual(newPoint))! {
                    withinBody = true
                    break
                }
                
                currentNode = currentNode?.next
            }
            
            if false == withinBody {
                break
            }
        }
        
        return newPoint
    }
    
    func didHitBody() -> Bool
    {
        var currentNode = self.snake.headNode.next
        var didHit: Bool = false
        while currentNode != nil {
            
            if currentNode!.point.isEqual(self.snake.headNode.point) {
                didHit = true
                break
            }
            
            currentNode = currentNode!.next
        }
        
        return didHit
    }
    
    func didHitFood() -> Bool
    {
        if self.snake.headNode.point.isEqual(self.food.point)
        {
            return true
        }
        
        return false
    }
    
    func endGame()
    {
        self.timer.invalidate()
        
        let alert = UIAlertController(title: "Game Over", message: "You scored \(self.score)!", preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "Ok!", style: UIAlertActionStyle.default) { UIAlertAction in
            
            let dispatchTime = DispatchTime.now()
            
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) { () -> Void in
                self.dismiss(animated: true)
                
            }
            
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true) { () -> Void in }
    }
    
    func swipe(_ gestureRecognizer: UISwipeGestureRecognizer)
    {
        let direction = gestureRecognizer.direction
        switch  direction {
        case UISwipeGestureRecognizerDirection.right:
            // snake can't move to right if it's already moving left
            if self.movementDirection != .Left {
                self.movementDirection = Movement.Right
            }
            
        case UISwipeGestureRecognizerDirection.left:
            // snake can't move to left if it's already moving right
            if self.movementDirection != .Right {
                self.movementDirection = Movement.Left
            }
            
        case UISwipeGestureRecognizerDirection.up:
            // snake can't move up if it's already moving down
            if self.movementDirection != .Down {
                self.movementDirection = Movement.Up
            }
            
        case UISwipeGestureRecognizerDirection.down:
            // snake can't move Down if it's already moving Up
            if self.movementDirection != .Up {
                self.movementDirection = Movement.Down
            }
            
        default:
            print ("unknonwn gesture direction")
        }
    }
    
    func didHitWall() -> Bool
    {
        
        if self.snake.headNode.point.x < 0 || self.snake.headNode.point.x >= worldWidth {
            return true
        }
        
        if self.snake.headNode.point.y < 0  || self.snake.headNode.point.y >= worldHeight{
            return true
        }
        
        return false
    }
}
